FROM gitpod/workspace-full

RUN sudo apt update -y
RUN sudo apt install -y ruby ruby-dev
RUN echo export PATH="${PATH}:/home/gitpod/.local/bin" >> /home/gitpod/.bashrc
RUN sudo curl -sSL https://get.haskellstack.org/ | sh
RUN mkdir -p /tmp/packages
RUN rm -Rf /tmp/packages/*
RUN echo hlint hoogle hindent hscolour > /tmp/packages/packages_0.txt
RUN apt-cache search ghc | grep -i haskell | awk -F" - " '{print $1}' >> /tmp/packages/packages_0.txt
RUN apt-cache search haskell | grep -i haskell | awk -F" - " '{print $1}' >> /tmp/packages/packages_0.txt
RUN sort /tmp/packages/packages_0.txt > /tmp/packages/packages_1.txt
RUN uniq /tmp/packages/packages_1.txt > /tmp/packages/packages_2.txt
RUN xargs sudo apt-get install --ignore-missing -y < /tmp/packages/packages_2.txt
